import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");
const expect = require("expect");


describe('Calculator tests', () => {
    let app;
    console.log("Tests started");


    test('Standart program test', (done) => {
        let program = "Standard";
        expect(calculateBonuses(program, 5000)).toBeCloseTo(0.05);
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.075);
        expect(calculateBonuses(program, 20000)).toBeCloseTo(0.075);
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.1);
        expect(calculateBonuses(program, 60000)).toBeCloseTo(0.1);
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.125);
        expect(calculateBonuses(program, 110000)).toBeCloseTo(0.125);
        done();
    });


    test('Premium program test', (done) => {
      let program = "Premium";
      expect(calculateBonuses(program, 5000)).toBeCloseTo(0.1);
      expect(calculateBonuses(program, 10000)).toBeCloseTo(0.15);
      expect(calculateBonuses(program, 20000)).toBeCloseTo(0.15);
      expect(calculateBonuses(program, 50000)).toBeCloseTo(0.2);
      expect(calculateBonuses(program, 60000)).toBeCloseTo(0.2);
      expect(calculateBonuses(program, 100000)).toBeCloseTo(0.25);
      expect(calculateBonuses(program, 110000)).toBeCloseTo(0.25);
      done();
    });


    test('Diamond program test', (done) => {
      let program = "Diamond";
      expect(calculateBonuses(program, 5000)).toBeCloseTo(0.2);
      expect(calculateBonuses(program, 10000)).toBeCloseTo(0.3);
      expect(calculateBonuses(program, 20000)).toBeCloseTo(0.3);
      expect(calculateBonuses(program, 50000)).toBeCloseTo(0.4);
      expect(calculateBonuses(program, 60000)).toBeCloseTo(0.4);
      expect(calculateBonuses(program, 100000)).toBeCloseTo(0.5);
      expect(calculateBonuses(program, 110000)).toBeCloseTo(0.5);
      done();
    });


    test('Invalid program test', (done) => {
      let program = "Invalid";
      expect(calculateBonuses(program, 5000)).toBeCloseTo(0);
      expect(calculateBonuses(program, 10000)).toBeCloseTo(0);
      expect(calculateBonuses(program, 20000)).toBeCloseTo(0);
      expect(calculateBonuses(program, 50000)).toBeCloseTo(0);
      expect(calculateBonuses(program, 60000)).toBeCloseTo(0);
      expect(calculateBonuses(program, 100000)).toBeCloseTo(0);
      expect(calculateBonuses(program, 110000)).toBeCloseTo(0);
      done();
    });

});
